/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnjoin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/24 15:23:23 by llenotre          #+#    #+#             */
/*   Updated: 2019/01/30 17:07:15 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	get_length(const size_t n, va_list args)
{
	va_list	a;
	size_t	i;
	size_t	j;

	va_copy(a, args);
	i = 0;
	j = 0;
	while (i < n)
	{
		j += ft_strlen(va_arg(a, const char*));
		++i;
	}
	va_end(a);
	return (j);
}

char			*ft_strnjoin(const size_t n, ...)
{
	va_list		args;
	size_t		i;
	size_t		j;
	char		*buffer;
	const char	*c;

	va_start(args, n);
	if (!(buffer = malloc(get_length(n, args) + 1)))
		return (NULL);
	i = 0;
	j = 0;
	while (i < n)
	{
		c = va_arg(args, const char*);
		ft_strcpy(buffer + j, c);
		++i;
		j += ft_strlen(c);
	}
	buffer[j] = '\0';
	va_end(args);
	return (buffer);
}
